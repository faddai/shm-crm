import os

from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.txt')).read()
CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()

requires = [
    'pyramid',
    'SQLAlchemy',
    'transaction',
    'pyramid_tm',
    'pyramid_debugtoolbar',
    'zope.sqlalchemy',
    'waitress',
    'pyramid_fanstatic',
    'colander',
    'pyramid_deform',
    'deform_bootstrap',
    'webhelpers',
    'pyramid_beaker',
    ]

setup(name='shm_crm',
      version='0.1',
      description='shm_crm',
      long_description=README + '\n\n' +  CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='',
      url='',
      keywords='web wsgi bfg pylons pyramid',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      test_suite='shm_crm',
      install_requires=requires,
      entry_points="""\
      [paste.app_factory]
      main = shm_crm:main
      [fanstatic.libraries]
      = resources:library
      [console_scripts]
      pserve-fanstatic = resources:pserve
      initialize_shm_crm_db = shm_crm.scripts.initializedb:main
      """,
      )

