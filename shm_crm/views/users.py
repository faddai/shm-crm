from pyramid.view import view_config
from pyramid.response import Response
from pyramid.httpexceptions import (HTTPFound, HTTPNotFound)

from pyramid_deform import FormView, CSRFSchema

from shm_crm.resources import DBSession as db
from shm_crm.resources.user import User
from shm_crm.layouts import Dashboard
from shm_crm.views import a_record_to_appstruct, merge_appstruct_with_record
from shm_crm.schemas.user import UserSchema

class Users(Dashboard):
    
    @view_config(route_name='view_users', renderer='../templates/users/view_users.pt')
    def view_all_users(self):
        users = db.query(User).all()
        page_title = 'Users'
        
        return {'users': users, 'title': page_title}
    
    @view_config(route_name='view_user', renderer='../templates/users/view_user.pt')
    def view_user(self):
        page_title = 'User Information'
        if self.request.matchdict.has_key('user_id'):
            user_id = self.request.matchdict.get('user_id')
            user = db.query(User).filter_by(id=user_id).first()
            if user is not None:
                return {'title': page_title, 'user': user}
            return Response("No user was found with the specified ID", content_type="text/html")
        return HTTPNotFound()
    
    @view_config(route_name='home', renderer='../templates/users/login.pt')
    @view_config(route_name='login', renderer='../templates/users/login.pt')
    def login(self):
        if 'submit_login' in self.request.POST:
            users_url = self.request.route_url('view_users')
            return HTTPFound(location=users_url)
        return {'login_url': self.request.route_url('login')}
    
    @view_config(route_name='logout')
    def logout(self):
        self.request.session.flash(u'You have successfully logged out')
        return HTTPFound(location=self.request.route_url('login'))
    
    @view_config(context=HTTPNotFound, renderer='../templates/404.pt')
    def not_found(self):
        self.request.response.status_int = 404
        return {}
    
    def __init__(self, request):
        self.request = request
        
# =====================
class UserEditView(FormView, Dashboard):
    schema = UserSchema()
    buttons = ('save',)
    form_options = (('formid', 'new-user'), ('method', 'POST'), ('bootstrap_form_style', 'form-vertical'))
    title = u'Edit User'
    record = User()
    
    def appstruct(self):
        if self.request.matchdict.has_key('user_id'):
            _id = int(self.request.matchdict['user_id'])
            self.record = db.query(User).filter_by(id=_id).first()
            if self.record is not None:
                return a_record_to_appstruct(self.record)
            raise HTTPNotFound()
    
    def save_success(self, appstruct):
        context = self.request.context
        """User id is required for the record update to be successful otherwise, a new recorded
        will be created"""
        appstruct['id'] = context.user_id
        record = merge_appstruct_with_record(self.record, appstruct)
        db.merge(record)
        db.flush()
        self.request.session.flash(u'Your changes were saved')
        return HTTPFound(location=self.request.route_url('view_users'))


class UserAddView(FormView, Dashboard):
    buttons = ('save',)
    schema = UserSchema()
    title = u'Add New User'
    
    def save_success(self, appstruct):
        new_user = User(**appstruct)
        db.add(new_user)
        self.request.session.flash(u'User was successfully added')
        return HTTPFound(location=self.request.route_url('view_users'))

