"""Turn a session object into a structure desirable by colander/deform, :appstruct"""
def a_record_to_appstruct(record):
    return dict([(k, record.__dict__[k]) for k in sorted(record.__dict__) if '_sa_' != k[:4]])

"""Turn back :appstruct into a formidable session object for persistence"""
def merge_appstruct_with_record(record, appstruct):
    for key, value in appstruct.iteritems():
        setattr(record, key, value)
    return record

