from pyramid.response import Response
from pyramid.view import view_config

from pyramid.httpexceptions import (HTTPFound, HTTPNotFound)

from webhelpers import constants

import deform

from pyramid_deform import FormView

from ..resources import DBSession as db
from ..resources.contact import (Contact, Phone)
from ..schemas.contact import ContactSchema
from ..layouts import Dashboard
from shm_crm.views import a_record_to_appstruct, merge_appstruct_with_record


class Contacts(Dashboard):
    
    @view_config(route_name='view_contacts', renderer="../templates/contacts/view_contacts.pt")
    def view_contacts(self):
        contacts = db.query(Contact).all()
        page_title = 'Contacts' 
        return {'contacts': contacts, 'title': page_title}
    
    @view_config(route_name='view_contact', renderer="../templates/contacts/view_contact.pt")
    def view_contact(self):
        if self.request.matchdict.has_key('contact_id'):
            contact_id = self.request.matchdict.get('contact_id')
            contact = db.query(Contact).filter_by(id=contact_id).first()
            if contact is None:
                return HTTPNotFound('We didn\'t find any contact with specified ID')
            return {'contact': contact, 'title': 'Contact Information'}
        return HTTPNotFound('You must provide a contact ID')
    
    def __init__(self, request):
        self.request = request

#=======================================
class ContactAddView(FormView, Dashboard):
    schema = ContactSchema()
    buttons = ('save',)
    title = u'Add New Contact'
    
    def save_success(self, appstruct):
        #print appstruct
        title = appstruct['personal_information']['title']
        firstname = appstruct['personal_information']['firstname']
        lastname = appstruct['personal_information']['lastname']

        #contact = Contact(**appstruct)
        contact = Contact(title, firstname, lastname)
        db.add(contact)
        self.request.session.flash(u'The Contact was successfully added')
        return HTTPFound(location=self.request.route_url('view_contacts'))

class ContactEditView(FormView, Dashboard):
    schema = ContactSchema()
    buttons = ('save',)
    form_options = (('formid', 'contact-add-form'),
                    ('method', 'POST'))
    record = Contact()
    title = u'Edit Contact'

    def save_success(self, appstruct):
        #print appstruct
        context = self.request.context
        # push back the record id into the session so that an update request will be
        # performed instead of creating a new record
        #appstruct['id'] = context.contact_id
        #record = merge_appstruct_with_record(self.record, appstruct)

        record = self.record
        record.id = context.contact_id
        record.title = appstruct['personal_information']['title']
        record.firstname = appstruct['personal_information']['firstname']
        record.lastname = appstruct['personal_information']['lastname']
        
        db.merge(record)
        db.flush()
        self.request.session.flash(u'Your changes were saved')
        return HTTPFound(location=self.request.route_url('view_contacts'))

    def appstruct(self):
        if self.request.matchdict.has_key('contact_id'):
            _id = int(self.request.matchdict.get('contact_id'))
            self.record = db.query(Contact).filter_by(id=_id).first()
            if self.record is not None:
                #return a_record_to_appstruct(self.record)
                struct = a_record_to_appstruct(self.record)
                struct['personal_information'] = struct
                #print struct
                return struct
            raise HTTPNotFound()
        # no id was passed along with this request, do something about it

