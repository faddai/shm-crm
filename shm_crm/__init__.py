from pyramid.config import Configurator
from sqlalchemy import engine_from_config

from pyramid_beaker import session_factory_from_settings

from shm_crm.resources import (DBSession, Base)
from shm_crm.views.contacts import (ContactEditView, ContactAddView)
from shm_crm.views.users import (UserAddView, UserEditView)

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    
    # sessions config
    config = Configurator(settings=settings)
    session_factory = session_factory_from_settings(settings)
    config.set_session_factory(session_factory)

    config.add_static_view('static', 'static', cache_max_age=3600)
    
    # routes
    config.add_route('home', '/')
    config.add_route('login', '/login')
    config.add_route('logout', '/logout')
    
    #--- Contact Views ----
    config.add_route('view_contacts', '/contacts')
    config.add_route('new_contact', '/contacts/new')
    config.add_route('view_contact', '/contacts/view/{contact_id}')
    config.add_route('edit_contact', '/contacts/edit/{contact_id}')
    config.add_route('delete_contact', '/contacts/delete/{contact_id}')

    config.add_view(ContactEditView, route_name='edit_contact', renderer='templates/form.pt')
    config.add_view(ContactAddView, route_name='new_contact', renderer='templates/form.pt')
    
    #--- Users Views ----
    config.add_route('view_users', '/users')
    config.add_route('new_user', '/users/new')
    config.add_route('view_user', '/users/view/{user_id}')
    config.add_route('edit_user', '/users/edit/{user_id}')
    config.add_route('delete_user', '/users/delete/{user_id}')
    
    config.add_view(UserEditView, route_name='edit_user', renderer='templates/form.pt')
    config.add_view(UserAddView, route_name='new_user', renderer='templates/form.pt')
    
    config.scan()
    return config.make_wsgi_app()

