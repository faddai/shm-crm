import unittest
import transaction

from pyramid import testing

from shm_crm.resources.contact import DBSession
from shm_crm.resources.contact import Contact

from datetime import date

class TestMyView(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()
        from sqlalchemy import create_engine
        engine = create_engine('sqlite://')
        from shm_crm.resources.contact import (
            Base,
            Contact,
            )
        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
        with transaction.manager:
            contact = Contact(title='mr',firstname='Francis', lastname='Addai',nationality='Ghanaian',dob=date(1988,6,6), interest='skiing,reading', marital_status='single')
            DBSession.add(contact)

    def tearDown(self):
        DBSession.remove()
        testing.tearDown()

    def test_it(self):
        from .views import view_contacts
        request = testing.DummyRequest()
        info = view_contacts(request)
        self.assertEqual(info['firstname'].name, 'Francis')
        self.assertEqual(info['nationality'], 'Ghanaian')
