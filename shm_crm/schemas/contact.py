import colander, deform, datetime

from deform.widget import SelectWidget
from deform.widget import TextAreaWidget
from deform.widget import DateInputWidget

import webhelpers.constants as constants

class PhoneSchema(colander.MappingSchema):
    
    types = (
        ('business', 'Business Mobile'),
        ('private', 'Private Mobile'),
        ('office', 'Office(direct line)'),
        ('assistant', 'Secretary / Assistant'),
        ('home', 'Home'),
        ('other', 'Other'))
        
    phone_type = colander.SchemaNode(
        colander.String(),
        title=u'Phone Type',
        widget=SelectWidget(values=types)
   )
    
    number = colander.SchemaNode(
        colander.String(),
        title=u'Phone Number'
   )
    
    fax = colander.SchemaNode(
        colander.String(),
        title=u'Fax Number'
   )

class PhonesSchema(colander.SequenceSchema):
    phone_numbers = PhoneSchema(title="Phone Information")

class EmailSchema(colander.MappingSchema):
    choices = (
        ('professional', 'Professional'),
        ('assistant', 'Secretary / Assistant'),
        ('personal', 'Personal'),
        ('other', 'Other'))
    
    email_type = colander.SchemaNode(
        colander.String(),
        title=u'Email Type',
        widget=SelectWidget(values=choices))
    
    email_address = colander.SchemaNode(
        colander.String(),
        title=u'Email Address')
    
class EmailsSchema(colander.SequenceSchema):
    emails = EmailSchema(title="Email Address")
    
class InstantMessagingAndSkypeSchema(colander.MappingSchema):
    choices = (
        ('skype', 'Skype'),
        ('msn', 'Windows Live Messenger (MSN Messenger)'),
        ('gtalk', 'Google Talk'),
        ('yahoo', 'Yahoo! Messenger'),
        ('bbm', 'Black Berry Messenger (BBM)'),
        ('other', 'Other'))
    
    type = colander.SchemaNode(
        colander.String(),
        title=u'Network / Service',
        widget=SelectWidget(values=choices))
    
    network_id = colander.SchemaNode(
        colander.String(),
        title=u'Nickname or ID',)
    
class IMSkypeSchema(colander.SequenceSchema):
    imskype = InstantMessagingAndSkypeSchema(title="Instant Messaging And Skype")

class SocialMediaAccountSchema(colander.MappingSchema):
    networks = (
        ('facebook', 'Facebook'),
        ('twitter', 'Twitter'),
        ('linkedin', 'Linkedin'),
        ('viadeo', 'Viadeo'),
        ('google+', 'Google+')
   )
    
    sharing_platforms = (
        ('youtube', 'Youtube'),
        ('vimeo', 'Vimeo'),
        ('slideshare', 'Slideshare'),
        ('other', 'Other'))
    
    
    social_network = colander.SchemaNode(
        colander.String(),
        title=u'Social Network',
        widget=SelectWidget(values=networks))
    
    account_id = colander.SchemaNode(
        colander.String(),
        title=u'Account ID')
    
    content_sharing_platform = colander.SchemaNode(
        colander.String(),
        title=u'Content Sharing Platform',
        widget=SelectWidget(values=sharing_platforms))
    
    account_url = colander.SchemaNode(
        colander.String(),
        title=u'Account URL')
    
class SocialMediaAccountsSchema(colander.SequenceSchema):
    social_media = SocialMediaAccountSchema(title="Social Media Account")
    
class WebsiteSchema(colander.MappingSchema):
    url = colander.SchemaNode(
        colander.String(),
        title=u'Website URL')
    
    name = colander.SchemaNode(
        colander.String(),
        title=u'Website name')

class TimePeriod(object):
    'Indicate beginning year and ending year'
    from_year = colander.SchemaNode(
        colander.Date(),
        title=u'From',
        widget=DateInputWidget())
    
    to_year = colander.SchemaNode(
        colander.Date(),
        title=u'From',
        widget=DateInputWidget())

class WebsitesSchema(colander.SequenceSchema):
    websites = WebsiteSchema(title="Website Address")

class CVEducationSchema(colander.MappingSchema, TimePeriod):
    school = colander.SchemaNode(
        colander.String(),
        title=u'School College/University')
    
    degree = colander.SchemaNode(
        colander.String(),
        title=u'Degree'
   )

class CVJobExperienceSchema(colander.MappingSchema, TimePeriod):
    company_name = colander.SchemaNode(
        colander.String(),
        title=u'Company / Organization name')
    
    job_title = colander.SchemaNode(
        colander.String(),
        title=u'Job Title / Position')
    
    'Specify beginning month and year and ending '
    time_period = colander.SchemaNode(
        colander.String(),
        title=u'Time Period',
        #widget=SelectWidget(values=)
   )
    
    'Description of the job experience'
    description = colander.SchemaNode(
        colander.String(),
        title=u'Description',
        description=u'Description of the job experience',
        widget=TextAreaWidget(rows=12, cols=8))
    
class CVSkillAndExpertise(colander.MappingSchema):
    skill_expertise = colander.SchemaNode(
        colander.String(),
        title=u'Skill, Expertise',
        widget=TextAreaWidget())

class CVEducationsSchema(colander.SequenceSchema):
    educations = CVEducationSchema(title=u'Education')

class CVJobExperiences(colander.SequenceSchema):
    job_experiences = CVJobExperienceSchema(title=u'Job Experience')

class CVSkillsAndExpertise(colander.SequenceSchema):
    skill_and_expertise = CVSkillAndExpertise(title=u'Skill and Expertise')

class CVSchema(colander.MappingSchema):
    education = CVEducationsSchema()
    job_experience = CVJobExperiences()
    skill_and_expertise = CVSkillsAndExpertise()

class PersonalInfoNoteSchema(colander.MappingSchema):
    note = colander.SchemaNode(
        colander.String(),
        title=u'Note',
        widget=TextAreaWidget(rows=12, cols=6),
        missing=u'',
        description=u'Add notes to profile information of this contact')
    
class PersonalInfoNotesSchema(colander.SequenceSchema):
    notes = PersonalInfoNoteSchema(title=u'Note')
    
class PersonalInfoSchema(colander.MappingSchema):
    titles = (
        ('Mr', 'Mr'),
        ('Mrs', 'Mrs'),
        ('Miss', 'Miss'),
        ('Dr', 'Dr'),
        ('Prof', 'Prof'),
        ('Ms', 'Ms'))
    
    title = colander.SchemaNode(
        colander.String(),
        title=u'Title',
        widget=SelectWidget(values=titles),)
    
    firstname = colander.SchemaNode(
        colander.String(),
        title=u'First Name',)
    
    lastname = colander.SchemaNode(
        colander.String(),
        title=u'Last Name',)
    
    nationality = colander.SchemaNode(
        colander.String(),
        title=u'Nationality',
        widget=SelectWidget(values=constants.country_codes()))
    
    languages = (
        ('en', 'English'),
        ('fr', 'French'),
        ('gb', 'German'))
    
    main_language = colander.SchemaNode(
        colander.String(),
        title=u'Main Language',
        widget=SelectWidget(values=languages))
    
    other_languages = colander.SchemaNode(
        colander.String(),
        title=u'Other language',
        missing=u'',
        widget=SelectWidget(values=languages, multiple=True))
    
    language_skill_levels = (
        ('bilingual', 'Bilingual'),
        ('basic', 'Basic'),
        ('beginner', 'Beginner'),
        ('intermediate', 'Intermediate'),
        ('advanced', 'Advanced'))
    
    language_skill_level = colander.SchemaNode(
        colander.String(),
        title=u'Language Skill Level',
        widget=SelectWidget(values=language_skill_levels))
    
    dob = colander.SchemaNode(
        colander.Date(),
        title=u'Date of Birth',
        widget=DateInputWidget())
    
    interests = colander.SchemaNode(
        colander.String(),
        title=u'Areas of interest, hobbies & sports',
        widget=TextAreaWidget(rows=10, cols=60),
        missing=u'')
    
    marital_status = colander.SchemaNode(
        colander.String(),
        title='Marital Status',
        widget=SelectWidget(values=(('single', 'Single'), ('married', 'Married'), ('divorced', 'Divorced'))),
        validator=colander.OneOf(('single','married','divorced')))
    
    notes = PersonalInfoNotesSchema()
    
"""
Generic schema for Main and Secondary Addresses
"""
class AddressSchema(colander.MappingSchema):
    for code, country in constants.country_codes():
        countries = (code, country.capitalize())
    
    country = colander.SchemaNode(
        colander.String(),
        title=u'Country',
        widget=SelectWidget(values=countries))
    
    regions = (
        ('ghana', 'Ghana'),
        ('geneva', 'Geneva')
   )
    
    region = colander.SchemaNode(
        colander.String(),
        title=u'Region',
        widget=SelectWidget(values=regions)
   )
    
    department = colander.SchemaNode(
        colander.String(),
        title=u'Department / Canton',
        widget=SelectWidget(values=(('a','A'), ('b', 'B')))
   )
    
    post_code_zip_code = colander.SchemaNode(
        colander.String(),
        title=u'Post code / Zip Code'
   )
    
    city = colander.SchemaNode(
        colander.String(),
        title=u'City',
        widget=SelectWidget(values=(('ksi', 'Kumasi')))
   )
    
    street = colander.SchemaNode(
        colander.String(),
        title=u'Street Address',
        widget=TextAreaWidget()
   )
    
    house_number = colander.SchemaNode(
        colander.String(),
        title=u'House Number',
        missing=u''
   )

"""
Inherits all fields from AddressSchema
"""
class MainAddressSchema(AddressSchema):
    pass

"""
Inherits all fields from AddressSchema
"""
class SecondaryAddressSchema(AddressSchema):
    pass

class OrganisationalOfficeSchema(colander.MappingSchema):
    'Can have many organisational offices. Show available organisational offices to choose from.'
    
    # values possibly obtained from a resource    
    offices = (
        ('Accra', 'Accra Office, Headquaters'),
        ('Kumasi','Kumasi Branch Office')
   )
    organisation_office = colander.SchemaNode(
        colander.String(),
        title=u'Organisational Office',
        widget=SelectWidget(values=offices))

class SecondaryAddressesSchema(colander.SequenceSchema):
    secondary_addresses = SecondaryAddressSchema(title=u'Secondary Address')

class OrganisationalOfficesSchema(colander.SequenceSchema):
    organisational_offices = OrganisationalOfficeSchema(title=u'Organisational Office')

class AddressesAndOfficesSchema(colander.MappingSchema):
    'Main Address'
    main = MainAddressSchema()
    'Secondary Address(es)'
    secondary = SecondaryAddressesSchema()
    'Organisational Offices'
    offices = OrganisationalOfficesSchema()

class ContactSchema(colander.MappingSchema):
    personal_information = PersonalInfoSchema()
    #addresses_and_offices = AddressesAndOfficesSchema()
    phone_information = PhonesSchema()
    email_addresses = EmailsSchema()
    instant_messaging_and_skype = IMSkypeSchema()
    social_media_accounts = SocialMediaAccountsSchema()
    website_addresses = WebsitesSchema()
    curriculum_vitae = CVSchema()
