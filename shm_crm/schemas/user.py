from colander import (MappingSchema, SchemaNode)
from colander import (String, Integer)

from deform.widget import SelectWidget
from deform.widget import PasswordWidget

class UserSchema(MappingSchema):
    username = SchemaNode(String())
    password = SchemaNode(String(),widget=PasswordWidget())
    firstname = SchemaNode(String())
    lastname = SchemaNode(String())
    group = SchemaNode(Integer(), widget=SelectWidget(values=((1, 'Admin:Organisation'),
                                                    (2, 'User:Organisation'),
                                                    (3, 'Super Admin'))))
