from sqlalchemy import (Column, Integer, Unicode,)
from sqlalchemy.orm import relationship, backref

from shm_crm.resources import Base, BaseObject

class User(Base, BaseObject):
    username = Column(Unicode(20), nullable=False)
    password = Column(Unicode(40), nullable=False)
    firstname = Column(Unicode(60), nullable=False)
    lastname = Column(Unicode(60), nullable=False)
    group = Column(Integer())
    
class Group(Base, BaseObject):
    pass
