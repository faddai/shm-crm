"""
Organise common import statements and make it available to any object that needs it
"""
import re
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.declarative import declared_attr

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    )

from zope.sqlalchemy import ZopeTransactionExtension

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()

def camel_case_to_name(text):
    """
      >>> camel_case_to_name('FooBar')
      'foo_bar'
      >>> camel_case_to_name('TXTFile')
      'txt_file'
      >>> camel_case_to_name ('MyTXTFile')
      'my_txt_file'
      >>> camel_case_to_name('froBOZ')
      'fro_boz'
      >>> camel_case_to_name('f')
      'f'
    """
    return re.sub(
        r'((?<=[a-z])[A-Z]|(?<!\A)[A-Z](?=[a-z]))', r'_\1', text).lower()

#def record_to_appstruct(self):
#    """
#    Transform a SQLAlchemy object into a deform compatible dict
#    usefull to autofill an editform directly from db recovered datas
#    """
#    return dict([(k, self.__dict__[k])
#                for k in sorted(self.__dict__) if '_sa_' != k[:4]])
#Base.appstruct = record_to_appstruct


class BaseObject(object):
            
    @declared_attr
    def __tablename__(cls):
        
        return "{0}s".format(camel_case_to_name(cls.__name__))
        
    @declared_attr
    def id(self):
        return sqlalchemy.Column('id', sqlalchemy.Integer, primary_key=True,
                                 autoincrement=True)