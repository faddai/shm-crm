from sqlalchemy import (Column, Integer, Text, Unicode, Date, ForeignKey)
from sqlalchemy.orm import relationship, backref

from . import Base, DBSession
from . import BaseObject

"""
Contact Resource Container definition
"""
class Contact(Base, BaseObject):
    title = Column(Unicode(20))
    firstname = Column(Unicode(60))
    lastname = Column(Unicode(60))
    nationality = Column(Unicode(60))
    dob = Column(Date())
    interest = Column(Text())
    marital_status = Column(Unicode(20))
    
    # adjust the __init__ method according to available columns
    def __init__(self,
                 title=u'',
                 firstname=u'',
                 lastname=u'',
                 nationality=u'',
                 dob=None,
                 interest=u'',
                 marital_status=u'',
                 curriculum_vitae=None,
                 website_addresses=None,
                 phone_information=None,
                 social_media_accounts=None,
                 personal_information=None,
                 instant_messaging_and_skype=None,
                 email_addresses=None):
        
        self.firstname = firstname
        self.lastname = lastname
        self.title = title
        self.nationality = nationality
        self.dob = dob
        self.interest = interest
        self.marital_status = marital_status

    def __repr__(self):
        return '<Contact(#%r. %r %r)>' % (self.id, self.firstname.encode(), self.lastname.encode())
    
    
class Email(BaseObject, Base):
    type = Column('email_type', Unicode(60))
    address = Column(Unicode(60))
    
    def __init__(self, address=u'', type=u''):
        self.type = type
        self.address = address
        
    def __repr__(self):
        return '<Email(%r, %r)>' % (self.type, self.address)
    
class InstantMessagingAndSkype(BaseObject, Base):
    __tablename__ = 'instant_messaging_and_skype'
    
    id = Column(Integer, primary_key=True)
    type = Column('im_type', Unicode(20))
    network_id = Column(Unicode(200))
    
class SocialMediaAccount(BaseObject, Base):
       
    social_network = Column(Unicode(20))
    social_network_id = Column(Unicode(60))
    sharing_platform = Column(Unicode(20))
    sharing_platform_account_URL = Column(Unicode(60))
    
class Website(BaseObject, Base):
    contact_id = Column(ForeignKey('contacts.id'))
    name = Column(Unicode(40))
    url = Column(Unicode(250))
    contact = relationship(Contact, backref=backref('websites'))
    
    def __init__(self, name=u'', url=u''):
        self.name = name
        self.url = url
        
    def __repr__(self):
        return '<Website(%r, %r)>' % (self.name, self.url)

"""
Education Information of a contact
This info is useful for the person's CV
"""
class EducationInfo(BaseObject, Base):
    __tablename__ = 'education_info'
    
    school = Column(Unicode(60))
    from_year = Column(Date)
    to_year = Column(Date)
    # time_period = Column(Unicode(20)) # tupleSequence ('FromYYYY', 'ToYYYY')
    degree = Column(Unicode(200))

"""
Job Experiences
Useful for the CV. Collect information on the experiences of the contact
"""
class JobExperience():
    __tablename__ = 'job_experience'
   
    company_name = Column(Unicode(60))
    job_title = Column(Unicode(20))
    time_period = Column(Unicode(20))
    description = Column(Text())
    
class SkillAndExpertise():
    __tablename__ = 'skills_and_expertise'
    
    skill_expertise = Column(Text())

class CV(BaseObject, Base):
    __tablename__ = 'cv'
    
    education_info_id = Column(ForeignKey('education_info.id'))
    job_experience = JobExperience()
    skill_and_expertise = SkillAndExpertise()
    
class PersonalInfoNote(Base, BaseObject):
    note = Column(Text())
    contact_id = Column(ForeignKey('contacts.id'))
    contact = relationship(Contact, backref=backref('personal_info_notes'))
    
    def __init__(self, note=u'', contact_id=None):
        self.note = note
        self.contact_id = contact_id
        
    def __repr__(self):
        return '<PersonalInfoNote(\'#%r. %r\') ContactID=%r>' % (self.id, self.note,
                                                                 self.contact_id)
    
class Language(BaseObject, Base):
    
    main_language = Column(Unicode(60))
    other_languages = Column(Unicode(60))
    language_skill_level = Column(Unicode(60))
    
"""
Not necessary for it to be a mapping that has a structure in the DB
It exists here to be inherited from by Main & Secondary Addresses
"""
class Address(object):
    country = Column(Unicode(60), nullable=False)
    region = Column(Unicode(60), nullable=False)
    department = Column(Unicode(60), nullable=False)
    post_code_zip_code = Column(Unicode(10))
    city = Column(Unicode(60), nullable=False)
    street = Column(Unicode(60))
    house_number = Column(Unicode(60))
    
  
    def __init__(self,
                 country=u'',
                 region=u'',
                 department=u'',
                 post_code_zip_code=u'',
                 city=u'',
                 street=u'',
                 house_number=u''):
        self.country = country
        self.city = city
        self.region = region
        self.department = department
        self.house_number = house_number
        self.post_code_zip_code = post_code_zip_code
        self.street = street
        
    def __repr__(self):
        return '<%r(%r, %r)>' % (self.__class__.__name__, self.country, self.region)
    
"""
There can be only one main address for the organisation
One-to-one relationship
"""
class MainAddress(Address, BaseObject, Base):
    __tablename__ = 'main_addresses'
    contact_id = Column(ForeignKey('contacts.id'))
    
    def __init__(self, **kwargs):
        super(MainAddress, self).__init__(**kwargs)
    
""" An organisation can have more than one secondary address 1-n"""
class SecondaryAddress(Address, BaseObject, Base):
    __tablename__ = 'secondary_addresses'
    contact_id = Column(ForeignKey('contacts.id'))
    contact = relationship(Contact, backref=backref('secondary_addresses'))
    
    def __init__(self, **kwargs):
        super(SecondaryAddress, self).__init__(**kwargs)
        

class Phone(BaseObject, Base):
    type = Column(Unicode(20))
    number = Column(Unicode(20))
    fax = Column(Unicode(20))
    contact_id = Column(ForeignKey('contacts.id'))
    
    """
    Create a bidirectional relationship a Contact & Phone information
    from Phone to Contact it's Many-to-one
    from Contact to Phone it's One-to-many
    """
    contact = relationship(Contact, backref=backref('phones', order_by=Contact.id))
    
    def __init__(self,
                 type=u"",
                 number=u"",
                 fax=u"",
                 contact_id=None):
        self.type = type
        self.number = number
        self.fax = fax
        self.contact_id = contact_id 

    def __repr__(self):
        return '<%r(type: %r, number: %r, fax: %r, contact: %r)>' % (self.__class__.__name__,
                                                                     self.type,
                                                                     self.number,
                                                                     self.fax,
                                                                     self.contact_id)