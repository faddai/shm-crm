from pyramid.renderers import get_renderer
from pyramid.decorator import reify

class Layouts(object):
    
    @reify
    def base(self):
        renderer = get_renderer('templates/base.pt')
        return renderer.implementation().macros['layout']
        
    @classmethod
    def page_title(self, title=''):
        company_name = 'SHM Research'        
        if title != '':
            return "%s | %s" % (title, company_name)
        return company_name

class Dashboard(object):
    @reify
    def dashboard_template(self):
        return get_renderer('templates/dashboard.pt').implementation().macros['dashboard'];

