import os
import sys
import transaction

from sqlalchemy import engine_from_config

from pyramid.paster import (
    get_appsettings,
    setup_logging,
    )

import datetime

from shm_crm.resources.contact import (
    DBSession,
    Contact, Phone, PersonalInfoNote, MainAddress,
    Base,
    )

def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri>\n'
          '(example: "%s development.ini")' % (cmd, cmd)) 
    sys.exit(1)

def main(argv=sys.argv):
    if len(argv) != 2:
        usage(argv)
    config_uri = argv[1]
    setup_logging(config_uri)
    settings = get_appsettings(config_uri)
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)
    with transaction.manager:
        
        phone = Phone(
            type=u"mobile",
            number=u"0244988988",
            fax=u"0240083979",
            contact_id=1
        )
        note = PersonalInfoNote(
            note="I am a note hidden in here",
            #contact_id
        )
        francis = Contact(title=u"Mr",
                        firstname=u"Francis",
                        lastname=u"Addai", 
                        nationality=u"Ghanaian",
                        dob=datetime.date(1988, 06, 06),
                        interest=u"music, programming, dancing, writing",
                        marital_status=u"single",
                        )
        
        gloria = Contact(title=u"Miss",
                        firstname=u"Gloria",
                        lastname=u"Agyare", 
                        nationality=u"Ghanaian",
                        dob=datetime.date(1989, 04, 26),
                        interest=u"music,dancing",
                        marital_status=u"attached",
                        )
        
        contacts = [francis, gloria]
        DBSession.add_all(contacts)
        DBSession.add(phone)
        DBSession.add(note)
        DBSession.add(MainAddress(country=u'Ghana',
                                  region=u'Ashanti',
                                  city=u'Kumasi',
                                  post_code_zip_code=u'233'))
